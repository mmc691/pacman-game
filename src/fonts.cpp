#include <iostream>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "game.h"
#include "Helpers.h"

GLuint font_tex;

FT_Library ftlib;
FT_Face face;
FT_GlyphSlot g;

VertexBufferObject FBO;
Eigen::Matrix<float, 5, 4> Glyph;

bool fonts_init(int pixel_size)
{
	if (FT_Init_FreeType(&ftlib)) {
		fprintf(stderr, "Could not init freetype library\n");
		return false;
	}

	if (FT_New_Face(ftlib, "../assets/emulogic.ttf", 0, &face)) {
		fprintf(stderr, "Could not open font\n");
		return false;
	}

	FT_Set_Pixel_Sizes(face, 0, pixel_size);

	glActiveTexture(GL_TEXTURE1);
	glGenTextures(1, &font_tex);
	glBindTexture(GL_TEXTURE_2D, font_tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	FBO.init();
	return true;
}

void fonts_render_text(const char *text, float x, float y, float w, float h, Program& program)
{
	const char *p;

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, font_tex);

	for(p = text; *p; p++) {
		if (FT_Load_Char(face, *p, FT_LOAD_RENDER))
			continue;

		g = face->glyph;

		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			g->bitmap.width,
			g->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			g->bitmap.buffer
		);

		float x1 = get_xpos_from_tile(x);
		float y1 = get_ypos_from_tile(y);

		Glyph.col(0) << x1, y1, -0.1, 0, 0;
		Glyph.col(1) << x1, y1 - h, -0.1, 0, 1;
		Glyph.col(2) << x1 + w, y1, -0.1, 1, 0;
		Glyph.col(3) << x1 + w, y1 - h, -0.1, 1, 1;

		FBO.update(Glyph);
		program.bindVertexAttribArray("position", FBO);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		x += 1;
	}
}

void fonts_cleanup(void)
{
	FBO.free();
	FT_Done_Face(face);
	FT_Done_FreeType(ftlib);
}
