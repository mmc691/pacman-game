#include <cmath>
#include <limits>
#include <vector>
#include <iostream>
#include <fstream>

// OpenGL Helpers to reduce the clutter
#include "Helpers.h"

// GLFW is necessary to handle the OpenGL context
#include <GLFW/glfw3.h>

// Linear Algebra Library
#include <Eigen/Core>
#include <Eigen/Dense>

#include "game.h"
#include "fonts.h"
#include "utils.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

Program program;

bool exit_prog = false;

// VertexBufferObject wrapper
VertexBufferObject SBO;
VertexBufferObject PBO;
VertexBufferObject WBO;

// Contains the vertex positions
Eigen::Matrix<float, 5, Eigen::Dynamic> Sprites;
Eigen::Matrix<float, 5, Eigen::Dynamic> Walls;
Eigen::Matrix<float, 5, Eigen::Dynamic> Pellets;

int width = 1, height = 1;
Eigen::Matrix4f identity = Eigen::Matrix4f::Identity(4, 4);
Eigen::Matrix4f view_trans = Eigen::Matrix4f::Identity(4, 4);

char scoreboard[1024];
unsigned score = 0, lives = 2, one_up = 0, kill_bonus = 20, pellets = 0;

std::vector<Sprite *> sprites;
Eigen::Matrix4f lives_transform[2];

Sprite pacman;

Sprite shadow;
Sprite speedy;
Sprite bashful;
Sprite pokey;

float tex_x_step, tex_y_step;

GLuint texture;
unsigned char *tex_img;
int img_width, img_height, img_channels;

int pacman_parts = 18;
unsigned direction_switch_ticks = 0, power_up_ticks = 0;

void update_sprites(void)
{
	Sprites.resize(5, 5 * 26);
	for (unsigned i = 0; i < 5; i++)
		Sprites.block<5, 26>(0, i * 26) << sprites[i]->matrix;

	SBO.update(Sprites);
}

void add_sprites(void)
{
	for (unsigned i = 0; i < map_height; i++) {
		for (unsigned j = 0; j < map_width; j++) {
			switch (tile_map[i][j]) {
			case 'P':
				pacman.initialize(j, i, 2.5 * tex_x_step, 4.5 * tex_y_step, tex_x_step, tex_y_step);
				pacman.color << 0.0, 1.0, 0.0, 1.0;
				pacman.rotate = true;
				pacman.has_eyes = false;
				break;
			case 'R':
				shadow.initialize(j, i, 2.5 * tex_x_step, 2.5 * tex_y_step, tex_x_step, tex_y_step);
				shadow.color << 1.0, 0.0, 0.0, 1.0;
				break;
			case 'C':
				bashful.initialize(j, i, 2.5 * tex_x_step, 2.5 * tex_y_step, tex_x_step, tex_y_step);
				bashful.color << 0.0, 1.0, 1.0, 1.0;
				break;
			case 'Y':
				speedy.initialize(j, i, 2.5 * tex_x_step, 2.5 * tex_y_step, tex_x_step, tex_y_step);
				speedy.color << 1.0, 0.75, 0.8, 1.0;
				break;
			case 'O':
				pokey.initialize(j, i, 2.5 * tex_x_step, 2.5 * tex_y_step, tex_x_step, tex_y_step);
				pokey.color << 1.0, 0.65, 0.0, 1.0;
				break;
			default:
				break;
			}
		}
	}

	shadow.add_eyes(2.0 * tex_x_step, 0.0 * tex_y_step, tex_x_step, tex_y_step);
	speedy.add_eyes(2.0 * tex_x_step, 0.0 * tex_y_step, tex_x_step, tex_y_step);
	bashful.add_eyes(2.0 * tex_x_step, 0.0 * tex_y_step, tex_x_step, tex_y_step);
	pokey.add_eyes(2.0 * tex_x_step, 0.0 * tex_y_step, tex_x_step, tex_y_step);

	sprites.push_back(&pacman);
	sprites.push_back(&shadow);
	sprites.push_back(&speedy);
	sprites.push_back(&bashful);
	sprites.push_back(&pokey);
	update_sprites();
}

void build_map(void)
{
	tile_width = 1.2f / float(map_width);
	tile_height = 1.6f / float(map_height);

	tex_x_step = 1.0f / 8.0f;
	tex_y_step = 1.0f / 6.0f;

	int walls = 0;
	for (unsigned i = 0; i < map_height; i++)
		for (unsigned j = 0; j < map_width; j++)
			if (tile_map[i][j] == '.' || tile_map[i][j] == 'o')
				pellets++;
			else if (not_walls.find(tile_map[i][j]) == std::string::npos)
				walls++;


	Walls.resize(5, 4 * walls);
	unsigned curr_col = 0;
	for (unsigned i = 0; i < map_height; i++) {
		for (unsigned j = 0; j < map_width; j++) {

			if (not_walls.find(tile_map[i][j]) != std::string::npos)
				continue;

			Walls.block<3, 1>(0, curr_col) << get_xpos_from_tile(j), get_ypos_from_tile(i), 0.0;
			Walls.block<3, 1>(0, curr_col + 1) << get_xpos_from_tile(j), get_ypos_from_tile(i + 1), 0.0;
			Walls.block<3, 1>(0, curr_col + 2) << get_xpos_from_tile(j + 1), get_ypos_from_tile(i), 0.0;
			Walls.block<3, 1>(0, curr_col + 3) << get_xpos_from_tile(j + 1), get_ypos_from_tile(i + 1), 0.0;
			switch (tile_map[i][j]) {
			case '-':
				Walls.block<2, 1>(3, curr_col) << 0.25 * tex_x_step, 5.25 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 0.25 * tex_x_step, 5.75 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 0.75 * tex_x_step, 5.25 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 0.75 * tex_x_step, 5.75 * tex_y_step;
				break;
			case '|':
				Walls.block<2, 1>(3, curr_col) << 0.25 * tex_x_step, 4.25 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 0.25 * tex_x_step, 4.75 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 0.75 * tex_x_step, 4.25 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 0.75 * tex_x_step, 4.75 * tex_y_step;
				break;
			case 'd':
				Walls.block<2, 1>(3, curr_col) << 0.5 * tex_x_step, 0.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 0.5 * tex_x_step, 1.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 1.0 * tex_x_step, 0.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 1.0 * tex_x_step, 1.0 * tex_y_step;
				break;
			case 'p':
				Walls.block<2, 1>(3, curr_col) << 0.0 * tex_x_step, 0.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 0.0 * tex_x_step, 0.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 0.5 * tex_x_step, 0.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 0.5 * tex_x_step, 0.5 * tex_y_step;
				break;
			case 'q':
				Walls.block<2, 1>(3, curr_col) << 0.5 * tex_x_step, 0.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 0.5 * tex_x_step, 0.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 1.0 * tex_x_step, 0.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 1.0 * tex_x_step, 0.5 * tex_y_step;
				break;
			case 'b':
				Walls.block<2, 1>(3, curr_col) << 0.0 * tex_x_step, 0.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 0.0 * tex_x_step, 1.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 0.5 * tex_x_step, 0.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 0.5 * tex_x_step, 1.0 * tex_y_step;
				break;
			case '=':
				Walls.block<2, 1>(3, curr_col) << 3.75 * tex_x_step, 0.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 3.75 * tex_x_step, 0.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 4.25 * tex_x_step, 0.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 4.25 * tex_x_step, 0.5 * tex_y_step;
				break;
			case 'Z':
				Walls.block<2, 1>(3, curr_col) << 3.75 * tex_x_step, 1.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 3.75 * tex_x_step, 1.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 4.25 * tex_x_step, 1.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 4.25 * tex_x_step, 1.5 * tex_y_step;
				break;
			case 'H':
				Walls.block<2, 1>(3, curr_col) << 3.25 * tex_x_step, 0.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 3.25 * tex_x_step, 1.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 3.75 * tex_x_step, 0.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 3.75 * tex_x_step, 1.0 * tex_y_step;
				break;
			case 'N':
				Walls.block<2, 1>(3, curr_col) << 4.25 * tex_x_step, 0.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 4.25 * tex_x_step, 1.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 4.75 * tex_x_step, 0.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 4.75 * tex_x_step, 1.0 * tex_y_step;
				break;
			case 'F':
				Walls.block<2, 1>(3, curr_col) << 3.25 * tex_x_step, 0.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 3.25 * tex_x_step, 0.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 3.75 * tex_x_step, 0.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 3.75 * tex_x_step, 0.5 * tex_y_step;
				break;
			case 'L':
				Walls.block<2, 1>(3, curr_col) << 3.25 * tex_x_step, 1.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 3.25 * tex_x_step, 1.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 3.75 * tex_x_step, 1.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 3.75 * tex_x_step, 1.5 * tex_y_step;
				break;
			case 'J':
				Walls.block<2, 1>(3, curr_col) << 4.25 * tex_x_step, 1.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 4.25 * tex_x_step, 1.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 4.75 * tex_x_step, 1.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 4.75 * tex_x_step, 1.5 * tex_y_step;
				break;
			case 'T':
				Walls.block<2, 1>(3, curr_col) << 4.25 * tex_x_step, 0.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 4.25 * tex_x_step, 0.5 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 4.75 * tex_x_step, 0.0 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 4.75 * tex_x_step, 0.5 * tex_y_step;
				break;
			case '_':
				Walls.block<2, 1>(3, curr_col) << 3.0 * tex_x_step, 3.125 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 1) << 3.0 * tex_x_step, 3.625 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 2) << 3.5 * tex_x_step, 3.125 * tex_y_step;
				Walls.block<2, 1>(3, curr_col + 3) << 3.5 * tex_x_step, 3.625 * tex_y_step;
				break;
			default:
				break;
			}

			curr_col += 4;
		}
	}
	WBO.update_static(Walls);
}

void update_pellets(void)
{
	Pellets.resize(5, 4 * pellets);
	unsigned curr_col = 0;
	for (unsigned i = 0; i < map_height; i++) {
		for (unsigned j = 0; j < map_width; j++) {
			if (!(tile_map[i][j] == '.' || tile_map[i][j] == 'o'))
				continue;
			Pellets.block<3, 1>(0, curr_col) << get_xpos_from_tile(j), get_ypos_from_tile(i), 0.0;
			Pellets.block<3, 1>(0, curr_col + 1) << get_xpos_from_tile(j), get_ypos_from_tile(i + 1), 0.0;
			Pellets.block<3, 1>(0, curr_col + 2) << get_xpos_from_tile(j + 1), get_ypos_from_tile(i), 0.0;
			Pellets.block<3, 1>(0, curr_col + 3) << get_xpos_from_tile(j + 1), get_ypos_from_tile(i + 1), 0.0;
			switch (tile_map[i][j]) {
			case 'o':
				Pellets.block<2, 1>(3, curr_col) << 0.25 * tex_x_step, 1.25 * tex_y_step;
				Pellets.block<2, 1>(3, curr_col + 1) << 0.25 * tex_x_step, 1.75 * tex_y_step;
				Pellets.block<2, 1>(3, curr_col + 2) << 0.75 * tex_x_step, 1.25 * tex_y_step;
				Pellets.block<2, 1>(3, curr_col + 3) << 0.75 * tex_x_step, 1.75 * tex_y_step;
				break;
			case '.':
				Pellets.block<2, 1>(3, curr_col) << 0.0 * tex_x_step, 1.0 * tex_y_step;
				Pellets.block<2, 1>(3, curr_col + 1) << 0.0 * tex_x_step, 2.0 * tex_y_step;
				Pellets.block<2, 1>(3, curr_col + 2) << 1.0 * tex_x_step, 1.0 * tex_y_step;
				Pellets.block<2, 1>(3, curr_col + 3) << 1.0 * tex_x_step, 2.0 * tex_y_step;
				break;
			default:
				break;
			}

			curr_col += 4;
		}
	}
	PBO.update(Pellets);
}

void framebuffer_size_callback(GLFWwindow* window, int width_, int height_)
{
	width = width_;
	height = height_;
	view_trans(0, 0) = float(height) / float(width);
	glViewport(0, 0, width, height);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (action != GLFW_PRESS)
		return;

	// Update the position of the first vertex if the keys 1,2, or 3 are pressed
	switch (key) {

	case GLFW_KEY_SPACE:
		if (game_state == W)
			game_state = S;
		break;

	case GLFW_KEY_W:
	case GLFW_KEY_UP:
		if (game_state == S)
			pacman.next_direction = U;
		break;
	case GLFW_KEY_S:
	case GLFW_KEY_DOWN:
		if (game_state == S)
			pacman.next_direction = D;
		break;
	case GLFW_KEY_A:
	case GLFW_KEY_LEFT:
		if (game_state == S)
			pacman.next_direction = L;
		break;
	case GLFW_KEY_D:
	case GLFW_KEY_RIGHT:
		if (game_state == S)
			pacman.next_direction = R;
		break;

	case GLFW_KEY_Y:
		if (game_state != F && game_state != V)
			return;

		if (lives > 0 && pellets > 0) {
			if (one_up)
				one_up--;
			else
				lives--;
			game_state = W;
			pacman_parts = 18;
			tock = 0;
			for (unsigned i = 0; i < sprites.size(); i++) {
				sprites[i]->cool_down_tock = 0;
				sprites[i]->reset();
			}
		} else {
			lives = 2;
			score = 0;
			one_up = 0;
			tick = 0;
			tock = 0;
			game_state = W;
			pacman_parts = 18;

			pellets = 0;
			free_tile_map(tile_map, map_width, map_height);
			tile_map = load_tile_map("../assets/tile-map.txt", map_width, map_height);

			build_map();
			update_pellets();

			for (unsigned i = 0; i < sprites.size(); i++)
				sprites[i]->reset();
		}
		break;

	case GLFW_KEY_N:
		if (game_state != F && game_state != V)
			return;
		exit_prog = true;
		break;

	default:
		break;
	}
}

void eat_pellet(int tile_x, int tile_y)
{
	score += tile_map[tile_y][tile_x] == '.' ? 1 : 5;
	if (tile_map[tile_y][tile_x] == 'o')
		power_up_ticks = 50;
	tile_map[tile_y][tile_x] = ' ';
	pellets--;
	update_pellets();
}

void update_game(void)
{
	if (tick % (10 - game_speed) != 0)
		return;
	tock++;

	int tile_x = int(pacman.centre(0));
	int tile_y = int(pacman.centre(1));
	unsigned old_score = score;

	switch (pacman.direction) {
	case L:
	case R:
		if (tile_x == pacman.centre(0)
				&& (tile_map[tile_y][tile_x] == '.' || tile_map[tile_y][tile_x] == 'o'))
			eat_pellet(tile_x, tile_y);
		break;
	case U:
	case D:
		if (tile_y == pacman.centre(1)
				&& (tile_map[tile_y][tile_x] == '.' || tile_map[tile_y][tile_x] == 'o'))
			eat_pellet(tile_x, tile_y);
		break;
	case X:
	default:
		break;
	}

	pacman_parts = (pacman_parts == 18 ? 16 : 18);

	if (pellets == 0) {
		game_state = V;
		return;
	}

	for (unsigned i = 1; i < sprites.size(); i++) {
		if ((pacman.centre - sprites[i]->centre).norm() < 1.0 && !sprites[i]->cool_down_tock) {
			if (power_up_ticks) {
				sprites[i]->cool_down_tock = 20;
				sprites[i]->reset();
				score += kill_bonus;
				kill_bonus *= 2;
			} else {
				game_state = K;
			}
		}
	}

	if (power_up_ticks > 0 && power_up_ticks % 2 == 0) {
		shadow.color << 1.0, 1.0, 1.0, 1.0;
		speedy.color << 1.0, 1.0, 1.0, 1.0;
		bashful.color << 1.0, 1.0, 1.0, 1.0;
		pokey.color << 1.0, 1.0, 1.0, 1.0;
		power_up_ticks--;
	} else if (power_up_ticks > 0 && power_up_ticks % 2 == 1) {
		shadow.color << 0.0, 0.0, 1.0, 1.0;
		speedy.color << 0.0, 0.0, 1.0, 1.0;
		bashful.color << 0.0, 0.0, 1.0, 1.0;
		pokey.color << 0.0, 0.0, 1.0, 1.0;
		power_up_ticks--;
	} else {
		shadow.color << 1.0, 0.0, 0.0, 1.0;
		speedy.color << 1.0, 0.75, 0.8, 1.0;
		bashful.color << 0.0, 1.0, 1.0, 1.0;
		pokey.color << 1.0, 0.65, 0.0, 1.0;
		kill_bonus = 20;
	}

	if (game_state == K) {
		for (unsigned i = 1; i < sprites.size(); i++)
			sprites[i]->cool_down_tock = 0;
		return;
	}


	// Update pacman
	if (pacman.next_direction != pacman.direction) {
		pacman.update(pacman.next_direction);
		if (pacman.next_direction != pacman.direction) {
			if (++direction_switch_ticks > 4)
				pacman.next_direction = pacman.direction;

		}
	} else {
		pacman.update(pacman.direction);
		pacman.next_direction = pacman.direction;
		direction_switch_ticks = 0;
	}

	// Update shadow
	if (shadow.cool_down_tock != 0)
		shadow.cool_down_tock--;
	else if (power_up_ticks)
		shadow.update(random_dir(shadow));
	else if (tock % 100 < 66)
		shadow.update(chase(shadow, pacman, 0));
	else
		shadow.update(scatter(shadow, map_width + 1, -1));

	// Update speedy
	if (speedy.cool_down_tock != 0)
		speedy.cool_down_tock--;
	else if ((tock - speedy.last_kill_tock) < 7)
		speedy.update_direction(U);
	else if (power_up_ticks)
		speedy.update(random_dir(speedy));
	else if (tock % 100 < 66)
		speedy.update(chase(speedy, pacman, 4));
	else
		speedy.update(scatter(speedy, -1, -1));

	// Update bashful
	if (bashful.cool_down_tock != 0)
		bashful.cool_down_tock--;
	else if ((tock - bashful.last_kill_tock) < 5)
		bashful.update_direction(R);
	else if ((tock - bashful.last_kill_tock) < 11)
		bashful.update_direction(U);
	else if (power_up_ticks)
		bashful.update(random_dir(bashful));
	else if (tock % 100 < 66)
		bashful.update(jitter_chase(bashful, shadow, pacman, 2));
	else
		bashful.update(scatter(bashful, map_width + 1, map_height + 1));

	// Update pokey
	if (pokey.cool_down_tock != 0)
		pokey.cool_down_tock--;
	else if ((tock - pokey.last_kill_tock) > 20 && (tock - pokey.last_kill_tock) < 25)
		pokey.update_direction(L);
	else if ((tock - pokey.last_kill_tock) >= 25 && (tock - pokey.last_kill_tock) < 31)
		pokey.update_direction(U);
	else if (power_up_ticks && (tock - pokey.last_kill_tock) > 20)
		pokey.update(random_dir(pokey));
	else if ((tock - pokey.last_kill_tock) > 20 && (tock % 100 < 66) && (pokey.centre - pacman.centre).norm() >= 8.0)
		pokey.update(scatter(pokey, -1, map_height + 1));
	else if ((tock - pokey.last_kill_tock) > 20 && tock % 100 < 66)
		pokey.update(chase(pokey, pacman, 0));
	else if ((tock - pokey.last_kill_tock) > 20)
		pokey.update(scatter(pokey, -1, map_height + 1));
	else
		pokey.update_direction(X);

	if ((tock % 100 == 66 || tock % 100 == 0) && !power_up_ticks) {
		shadow.update(reverse_dir(shadow.direction));
		if (speedy.cool_down_tock == 0 && (tock - speedy.last_kill_tock) > 7)
			speedy.update(reverse_dir(speedy.direction));
		if (bashful.cool_down_tock == 0 && (tock - bashful.last_kill_tock) > 11)
			bashful.update(reverse_dir(bashful.direction));
		if (pokey.cool_down_tock == 0 && (tock - pokey.last_kill_tock) > 31)
			pokey.update(reverse_dir(pokey.direction));
	}

	if ((old_score % 200) > (score % 200)) {
		if (lives == 2)
			one_up++;
		else
			lives++;
	}
}

void finish_game(void)
{
	if (pellets == 0)
		return;

	if (tick % (10 - game_speed) != 0)
		return;

	if (pacman_parts)
		pacman_parts--;
	else
		game_state = F;
}

int main(void)
{
	GLFWwindow* window;
	unsigned int curr_col;

	// Initialize the library
	if (!glfwInit())
		return -1;

	// Activate supersampling
	glfwWindowHint(GLFW_SAMPLES, 8);

	// Ensure that we get at least a 3.2 context
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);

	// On apple we have to load a core profile with forward compatibility
#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	// Create a windowed mode window and its OpenGL context
	window = glfwCreateWindow(960, 720, "Pacman", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	// Make the window's context current
	glfwMakeContextCurrent(window);

#ifndef __APPLE__
	glewExperimental = true;
	GLenum err = glewInit();
	if(GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}
	glGetError(); // pull and savely ignonre unhandled errors like GL_INVALID_ENUM
#endif

	// Initialize the VAO
	// A Vertex Array Object (or VAO) is an object that describes how the vertex
	// attributes are stored in a Vertex Buffer Object (or VBO). This means that
	// the VAO is not the actual object storing the vertex data,
	// but the descriptor of the vertex data.
	VertexArrayObject VAO;
	VAO.init();
	VAO.bind();

	// Initialize the VBO with the vertices data
	// A VBO is a data container that lives in the GPU memory
	SBO.init();
	PBO.init();
	WBO.init();

	// Initialize the OpenGL Program
	// A program controls the OpenGL pipeline and it must contains
	// at least a vertex shader and a fragment shader to be valid
	const GLchar* vertex_shader =
		"#version 150 core\n"
		"in vec3 position;"
		"in vec2 tex_coord;"
		"uniform mat4 mmodel;"
		"uniform mat4 mview;"
		"out vec2 TexCoord;"
		"void main()"
		"{"
		"    gl_Position = mview * mmodel * vec4(position, 1.0);"
		"    TexCoord = tex_coord;"
		"}";
	const GLchar* fragment_shader =
		"#version 150 core\n"
		"in vec2 TexCoord;"
		"uniform vec3 mcolor;"
		"uniform sampler2D Tex;"
		"uniform sampler2D Font;"
		"uniform int use_tex;"
		"out vec4 outColor;"
		"vec4 tex;"
		"void main()"
		"{"
		"    tex = use_tex == 1 ? texture2D(Tex, TexCoord) : vec4(1, 1, 1, texture2D(Font, TexCoord).r);"
		"    outColor = vec4(tex.r * mcolor.r, tex.g * mcolor.g, tex.b * mcolor.b, tex.a);"
		"}";

	// Compile the two shaders and upload the binary to the GPU
	// Note that we have to explicitly specify that the output "slot" called outColor
	// is the one that we want in the fragment buffer (and thus on screen)
	program.init(vertex_shader, fragment_shader, "outColor");
	program.bind();

	// Register the keyboard callback
	glfwSetKeyCallback(window, key_callback);

	// Update viewport
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	// Texture generation
	tex_img = load_image("../assets/texture.png", &img_width, &img_height, &img_channels);
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img_width, img_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex_img);
	stbi_image_free(tex_img);

	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glfwGetWindowSize(window, &width, &height);
        view_trans(0, 0) = float(height) / float(width);

	tile_map = load_tile_map("../assets/tile-map.txt", map_width, map_height);
	build_map();
	update_pellets();
	add_sprites();

	lives_transform[0] << identity;
	lives_transform[0](0, 3) = get_xpos_from_tile(2);
	lives_transform[0](1, 3) = get_ypos_from_tile(map_height + 1);
	lives_transform[1] << identity;
	lives_transform[1](0, 3) = get_xpos_from_tile(4);
	lives_transform[1](1, 3) = get_ypos_from_tile(map_height + 1);

	if (!fonts_init(20))
		goto exit_lbl;

	curr_col = 0;
	while (!glfwWindowShouldClose(window) && !exit_prog)
	{
		// Bind your program
		program.bind();

		// Clear the framebuffer
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);

		// Upload view transformation and set model transform to identity for objects
		glUniformMatrix4fv(program.uniform("mview"), 1, GL_FALSE, view_trans.data());
		glUniformMatrix4fv(program.uniform("mmodel"), 1, GL_FALSE, identity.data());
		glUniform1i(program.uniform("Tex"), 0);
		glUniform1i(program.uniform("Font"), 1);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		glUniform1i(program.uniform("use_tex"), 1);

		// Render the walls
		program.bindVertexAttribArray("position", WBO);
		glUniform3f(program.uniform("mcolor"), 1.0, 1.0, 1.0);
		for (unsigned i = 0; i < Walls.cols(); i += 4)
			glDrawArrays(GL_TRIANGLE_STRIP, i, 4);

		// Render the pellets
		program.bindVertexAttribArray("position", PBO);
		glUniform3f(program.uniform("mcolor"), 0.0, 1.0, 0.0);
		for (unsigned i = 0; i < Pellets.cols(); i += 4)
			glDrawArrays(GL_TRIANGLE_STRIP, i, 4);

		// Render the sprites
		curr_col = 0;
		update_sprites();
		program.bindVertexAttribArray("position", SBO);
		for (unsigned i = 0; i < sprites.size(); i++) {
			if (sprites[i]->cool_down_tock)
				continue;

			glUniformMatrix4fv(program.uniform("mmodel"), 1, GL_FALSE, sprites[i]->transform.data());
			glUniform3f(program.uniform("mcolor"),
					sprites[i]->color(0), sprites[i]->color(1),
					sprites[i]->color(2));
			if (i == 0) {
				glDrawArrays(GL_TRIANGLE_FAN, curr_col, pacman_parts);
			} else {
				glDrawArrays(GL_TRIANGLE_FAN, curr_col, 18);

				glUniformMatrix4fv(program.uniform("mmodel"), 1, GL_FALSE, sprites[i]->eye_transform.data());
				glUniform3f(program.uniform("mcolor"), 1.0, 1.0, 1.0);
				glDrawArrays(GL_TRIANGLE_STRIP, curr_col + 18, 4);
				glDrawArrays(GL_TRIANGLE_STRIP, curr_col + 22, 4);
			}
			curr_col += 26;
		}

		glUniform3f(program.uniform("mcolor"), pacman.color(0), pacman.color(1), pacman.color(2));
		for (unsigned i = 0; i < lives; i++) {
			glUniformMatrix4fv(program.uniform("mmodel"), 1, GL_FALSE, (lives_transform[i]).data());
			glDrawArrays(GL_TRIANGLE_FAN, 0, 16);
		}

		glUniform3f(program.uniform("mcolor"), 0.0, 1.0, 0.0);
		glUniformMatrix4fv(program.uniform("mmodel"), 1, GL_FALSE, identity.data());
		glUniform1i(program.uniform("use_tex"), 0);

		fonts_render_text("1UP         SCORE", 3, -3.25, tile_width, tile_height, program);
		sprintf(scoreboard, "  %02d%10d0", one_up, score);
		fonts_render_text(scoreboard, 3, -2, tile_width, tile_height, program);

		if (game_state == W)
			fonts_render_text("READY!", 11, 17, tile_width, tile_height, program);
		else if (game_state == F && lives > 0)
			fonts_render_text("CONTINUE? (Y/N)", 7.5, -1, tile_width, tile_height, program);
		else if (game_state == V || (game_state == F && lives == 0))
			fonts_render_text("PLAY AGAIN? (Y/N)", 5.5, -1, tile_width, tile_height, program);

		// Swap front and back buffers
		glfwSwapBuffers(window);

		// Poll for and process events
		glfwPollEvents();
		tick++;
		if (game_state == S)
			update_game();
		else if (game_state == K)
			finish_game();
	}

exit_lbl:
	// Deallocate opengl memory
	program.free();
	VAO.free();
	SBO.free();
	PBO.free();
	WBO.free();

	fonts_cleanup();

	free_tile_map(tile_map, map_width, map_height);

	// Deallocate glfw internals
	glfwTerminate();
	return 0;
}
