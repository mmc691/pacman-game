#ifndef FONTS_H
#define FONTS_H

#include <ft2build.h>
#include FT_FREETYPE_H

#include "Helpers.h"

extern FT_Library ftlib;
extern FT_Face face;
extern GLuint font_tex;
extern VertexBufferObject FBO;

bool fonts_init(int pixel_size);
void fonts_cleanup(void);

void fonts_render_text(const char *text, float x, float y, float sx, float sy, Program& program);

#endif
