#ifndef GAME_H
#define GAME_H

#include <vector>

#include <Eigen/Core>

enum Direction {
	X = 0,
	U = 'U',
	D = 'D',
	L = 'L',
	R = 'R',
};

enum State {
	W = 'W',
	S = 'S',
	K = 'K',
	V = 'V',
	F = 'F',
};

extern State game_state;
extern const unsigned int game_speed;
extern unsigned int tick, tock;

extern char **tile_map;
extern int map_width; extern int map_height;
extern float tile_width; extern float tile_height;

static const std::string not_walls = ".o PRCYO";

class Sprite
{
public:
	Eigen::Vector4f color;
	Eigen::Matrix4f transform;
	Eigen::Matrix<float, 5, 26> matrix;

	bool rotate = false;
	bool has_eyes = true;
	Eigen::Matrix4f eye_transform;

	Eigen::Vector2f centre;
	Eigen::Vector2f init_position;
	enum Direction direction;
	enum Direction next_direction;

	unsigned int last_kill_tock = 0;
	unsigned int cool_down_tock = 0;

	Sprite(void) { }

	void initialize(int x, int y, float tex_x, float tex_y, float tex_width, float tex_height);
	void update_direction(Direction new_direction);
	void update(Direction new_direction);
	void reset(void);
	void add_eyes(float tex_x, float tex_y, float tex_width, float tex_height);
};

bool hits_wall(const Sprite& sprite, const Direction direction);

char **load_tile_map(const char *fname, int& width, int& height);
void free_tile_map(char **map, int width, int height);

static inline float get_xpos_from_tile(float tile_x)
{
	return tile_width * tile_x - 0.6;
}

static inline float get_ypos_from_tile(float tile_y)
{
	return 0.8 - tile_height * tile_y;
}

enum Direction reverse_dir(enum Direction dir);
enum Direction random_dir(const Sprite& sprite);
enum Direction chase(const Sprite& sprite, const Sprite& pacman, int tile_delta);
enum Direction jitter_chase(const Sprite& sprite, const Sprite& jitter, const Sprite& pacman, int tile_delta);
enum Direction scatter(const Sprite& sprite, float target_x, float target_y);

#endif
